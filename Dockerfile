FROM alpine:3.4
MAINTAINER Nosebit Dev Team <dev@nosebit.com>

# Install dev dependencies
RUN build_pkgs="bash" && \
    run_pkgs="ca-certificates openssl supervisor" && \
    apk --update add ${build_pkgs} ${run_pkgs}

# Download and install consul-template
RUN mkdir -p /tmp && \
    wget -q -O /tmp/consul-template.zip https://releases.hashicorp.com/consul-template/0.15.0/consul-template_0.15.0_linux_amd64.zip && \
    unzip /tmp/consul-template.zip -d /usr/bin

# Set working directory
WORKDIR /home

# Add files
ADD conf ./conf
ADD start.sh ./
ADD supervisord.conf /etc/supervisord.conf

# Make scripts executables
RUN chmod u+x start.sh && \
    apk del ${build_pkgs}

# The main entrypoint
CMD ["/home/start.sh"]
